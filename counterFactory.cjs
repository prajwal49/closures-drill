function counterFactory() {
    
    var counter = 0;
    return {
        increment(){
           return ++counter
        },
        decrement(){
           return --counter
        }
    }


}

module.exports = counterFactory

