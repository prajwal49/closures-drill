const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

// callback function
function testFunction() {
    return 'Function called';
}

// limited function that can be called at most 3 times
const limitedFunction = limitFunctionCallCount(testFunction, 3);

console.log(limitedFunction()); 
console.log(limitedFunction()); 
console.log(limitedFunction()); 
console.log(limitedFunction()); 
