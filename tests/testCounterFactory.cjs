const counterFactory = require("../counterFactory.cjs");

try {
  const counter = counterFactory();

  //increment
  console.log(counter.increment());
  console.log(counter.increment());
  console.log(counter.increment());

  //decrement
  console.log(counter.decrement());
  console.log(counter.decrement());
  console.log(counter.decrement());
} catch (error) {
    console.log(error.message);
}
