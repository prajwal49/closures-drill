const cacheFunction = require('../cacheFunction.cjs')

const add = (a, b) => a + b;
const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1, 2)); 
console.log(cachedAdd(1, 2)); // return by cache when we get same result
console.log(cachedAdd(2, 3)); 
console.log(cachedAdd(2, 3)); // return by cache when we get same result
