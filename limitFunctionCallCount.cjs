function limitFunctionCallCount(cb, n) {
    if (typeof cb !== 'function') {
        throw new TypeError('First argument must be a function');
    }
    if (typeof n !== 'number' || n < 1) {
        throw new TypeError('Second argument must be a positive number');
    }
    let callCount = 0;

    return function(...args){
        if(callCount < n){
            callCount++;
            return cb(...args);
        }
        return null;
    }
    
}

module.exports = limitFunctionCallCount