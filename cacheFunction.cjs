function cacheFunction(cb) {
  if (typeof cb !== "function") {
    throw new TypeError("Argument must be a function");
  }
  const cache = {};

  return function (...args) {
    const key = JSON.stringify(args);
    if (!(key in cache)) {
      cache[key] = cb(...args);
    }
    return cache[key];
  };
}

module.exports = cacheFunction;
